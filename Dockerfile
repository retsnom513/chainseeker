FROM node:lts-alpine3.20

# setup app dirs
RUN mkdir /app
WORKDIR /app

# bring in the chainseeker dirs
COPY client /app/client
COPY server /app/server
COPY entrypoint.sh /usr/bin/entrypoint.sh

# install the server
RUN cd /app/server \
    && /usr/local/bin/npm install \
    && /usr/local/bin/npm audit fix

# install the client
RUN cd /app/client \
    && /usr/local/bin/npm install \
    && /usr/local/bin/npm audit fix

ENTRYPOINT ["/bin/sh", "/usr/bin/entrypoint.sh"]